import pandas as pd
import numpy as np
import math
import random

from sklearn.preprocessing import LabelEncoder
from sklearn.naive_bayes import GaussianNB

import itertools

import matplotlib.pyplot as plt
from fairlearn.metrics import (
    MetricFrame,
    true_positive_rate,
    true_negative_rate,
    false_positive_rate,
    false_negative_rate,
    selection_rate,
    count,
) 

########## 공통 ##########

def main(static_data, test_data, target, subgroup, mode):
    if mode=='static' : 
        cols = list(static_data.columns)
        except_target = cols.copy()
        except_target.remove(target)
        
        _, _, before_info_a, before_info_b = get_info(static_data, test_data, target, subgroup, except_target)

        fair_data, avg_fair = get_first_data(static_data, target, before_info_a[0], before_info_b[0], before_info_a[1], before_info_b[1])
        for i in except_target : 
            avg_fair, fair_data = static_fair(avg_fair, fair_data, i)

        fair=pd.DataFrame()
        for i,j in zip(fair_data,avg_fair) :
            fair = pd.concat([i.sample(int(j)), fair])
        
        return fair
    
    elif mode=='dynamic' : 
        num=1
        cols = list(static_data.columns)
        except_target = cols.copy()
        except_target.remove(target)
        
        remain=pd.DataFrame()
        train, remain, num = dynamic_fair(static_data, remain, num, target)
        

        for num in range(2,4,1) : 
            batch = pd.read_csv('batch_'+str(num)+'.csv')
            remain = pd.concat([remain, batch])

            train, remain, num = dynamic_fair(train, remain, num, target)
        
        return train, remain
    
    else : 
        print('Wrong Command')
        
        
def verification(before_data, after_data, test_data, target, subgroup):
    
    cols = list(before_data.columns)
    except_target = cols.copy()
    except_target.remove(target)
    
    before_matrix_a, before_matrix_b, before_info_a, before_info_b = get_info(before_data, test_data, target, subgroup, except_target)
    after_matrix_a, after_matrix_b, after_info_a, after_info_b = get_info(after_data, test_data, target, subgroup, except_target)
    
    score_TPR = (abs(before_info_a[0]-before_info_b[0])-abs(after_info_a[0]-after_info_b[0]))/abs(before_info_a[0]-before_info_b[0])
    # print("Equality of Opportunity : " + str(score_TPR*100) + " %")

    score_TPRFPR = ((abs(before_info_a[0]-before_info_b[0])-abs(after_info_a[0]-after_info_b[0]))/abs(before_info_a[0]-before_info_b[0]) + 
                    (abs(before_info_a[1]-before_info_b[1])-abs(after_info_a[1]-after_info_b[1]))/abs(before_info_a[1]-before_info_b[1]))/2
    # print("Equalized odds : " + str(score_TPRFPR*100) + " %")

    score_ED = (abs(before_info_a[3]-before_info_b[3])-abs(after_info_a[3]-after_info_b[3]))/abs(before_info_a[3]-before_info_b[3])
    # print("Demographic Parity : " + str(score_DP*100) + " %")

    score_TE = (abs(before_info_a[4]-before_info_b[4])-abs(after_info_a[4]-after_info_b[4]))/abs(before_info_a[4]-before_info_b[4])
    # print("Treatment Equal : " + str(score_TR*100) + " %")
    
    score = [score_TPR, score_TPRFPR, score_ED, score_TE]
    

    avg_score = (score_TPR+score_TPRFPR+score_ED+score_TE)/4
#     print(
#           " Avg : "+str(avg_score*100)[:5] + " %" 
#         + " Equal Opportunity : "+str(score_TPR*100)[:5] + " %" 
#         + " Equalized Odd : "+str(score_TPRFPR*100)[:5] + " %" 
#         + " Equalizing Disincentives : "+str(score_ED*100)[:5] + " %" 
#         + " Treatment Equal : "+str(score_TE*100)[:5] + " %"
#          )
#     print("--------------------------------------------------------")
    
    before_result = [before_matrix_a, before_matrix_b, before_info_a, before_info_b]
    after_result = [after_matrix_a, after_matrix_b, after_info_a, after_info_a]
    
    return before_result, after_result, score


def print_score(score) : 
    print(
        " Equal Opportunity : "+str(score[0]) +'\n'
        + " Equalized Odd : "+str(score[1]) +'\n'
        + " Equalizing Disincentives : "+str(score[2]) +'\n'
        + " Treatment Equal : "+str(score[3])
         )
    print("--------------------------------------------------------")


def draw_result(before_data, after_data, test_data, label, subgroup) : 
#### Before
    y_train = before_data[label]#(before_data[label] == before_data[label].unique()[0]) * 1
    X_train = before_data.drop(label, axis=1)

    classifier = GaussianNB()
    classifier.fit(X_train, y_train)
    
    X_test = test_data.drop(label, axis=1)
    y_test = test_data[label]
    
    y_pred = classifier.predict(X_test)
    sensitive_features = X_test[subgroup]

    metrics = {
        "true positive rate": true_positive_rate,
        "true negative rate": true_negative_rate,
        "false positive rate": false_positive_rate,
        "false negative rate": false_negative_rate,
    }

    metric_frame = MetricFrame(
        metrics=metrics, y_true=y_test, y_pred=y_pred, sensitive_features=sensitive_features
    )

    before = metric_frame.by_group

    metric_frame.by_group.plot.bar(
        subplots=True,
        layout=[1, 4],
        legend=False,
        figsize=[8, 4],
        title="Before Remove Bias",
        ylim=[0, 1]
    )


#### After
    y_train2 = after_data[label] # (after_data[label] == after_data[label].unique()[0]) * 1
    X_train2 = after_data.drop(label, axis=1)

    classifier2 = GaussianNB()
    classifier2.fit(X_train2, y_train2)
    
    y_pred2 = classifier2.predict(X_test)
    
    metrics2 = {
        "true positive rate": true_positive_rate,
        "true negative rate": true_negative_rate,
        "false positive rate": false_positive_rate,
        "false negative rate": false_negative_rate,

    }

    metric_frame = MetricFrame(
        metrics=metrics2, y_true=y_test, y_pred=y_pred2, sensitive_features=sensitive_features
    )

    after = metric_frame.by_group

    plt = metric_frame.by_group.plot.bar(
        subplots=True,
        layout=[1, 4],
        legend=False,
        figsize=[8, 4],
        title="After Remove Bias",
        ylim=[0, 1]
        
    )
         

def print_plt(eqop_list, eqod_list, eqdi_list, treq_list) : 
    x = range(len(eqop_list))
    
    plt.plot(x, eqop_list)
    plt.plot(x, eqod_list)
    plt.plot(x, eqdi_list)
    plt.plot(x, treq_list)
    plt.legend(['eqop', 'eqod', 'eqdi', 'treq'])
    
    plt.show()
    
    
    

def get_matrix(y_test, y_hat) : 
    tp = np.sum((y_test ==1) & (y_hat==1) )
    tn = np.sum((y_test ==0) & (y_hat==0) )
    fp = np.sum((y_test ==0) & (y_hat==1) )
    fn = np.sum((y_test ==1) & (y_hat==0) )
    
    #accuracy = np.mean(np.equal(y_test,y_hat))
    
    return tp, tn, fp, fn




def get_info(train_data, test_data, target, subgroup, except_target) : 
    y_train=train_data[target].astype(int)
    X_train=train_data[except_target].astype(int)

    CATEGORY  =  subgroup
    SUBGROUP = 0 
    X_test_a  = test_data.loc[test_data[CATEGORY] == SUBGROUP][except_target]
    y_test_a  = test_data.loc[test_data[CATEGORY] == SUBGROUP][target]

    SUBGROUP = 1 
    X_test_b  = test_data.loc[test_data[CATEGORY] == SUBGROUP][except_target]
    y_test_b  = test_data.loc[test_data[CATEGORY] == SUBGROUP][target]

    model = GaussianNB()
    model.fit(X_train, y_train)

    y_hat = model.predict(X_test_a)
    before_tp_a, before_tn_a, before_fp_a, before_fn_a = get_matrix(y_test_a, y_hat)

    before_tpr_a = before_tp_a/(before_tp_a+before_fn_a)
    before_fpr_a = before_fp_a/(before_fp_a+before_tn_a)
    before_fnr_a = before_fn_a/(before_tp_a+before_fn_a)

    before_ed_a = ((before_tp_a)/(before_fn_a+before_tp_a))-((before_fp_a)/(before_fp_a+before_tn_a))
    before_te_a = before_fpr_a / before_fnr_a
    
    
    y_hat = model.predict(X_test_b)
    before_tp_b, before_tn_b, before_fp_b, before_fn_b = get_matrix(y_test_b, y_hat)

    before_tpr_b = before_tp_b/(before_tp_b+before_fn_b)
    before_fpr_b = before_fp_b/(before_fp_b+before_tn_b)
    before_fnr_b = before_fn_b/(before_tp_b+before_fn_b)

    before_ed_b = ((before_tp_b)/(before_fn_b+before_tp_b))-((before_fp_b)/(before_fp_b+before_tn_b))
    before_te_b = before_fpr_b / before_fnr_b
    
    
    matrix_a = [ before_tp_a, before_tn_a, before_fp_a, before_fn_a ]
    matrix_b = [ before_tp_b, before_tn_b, before_fp_b, before_fn_b ]
    
    info_a = [before_tpr_a, before_fpr_a, before_fnr_a, before_ed_a, before_te_a]
    info_b = [before_tpr_b, before_fpr_b, before_fnr_b, before_ed_b, before_te_b]
    
    return matrix_a, matrix_b, info_a, info_b


# def print_score(before_info_a, before_info_b, after_info_a, after_info_b) : 
#     score_TPR = (abs(before_info_a[0]-before_info_b[0])-abs(after_info_a[0]-after_info_b[0]))/abs(before_info_a[0]-before_info_b[0])
#     # print("Equality of Opportunity : " + str(score_TPR*100) + " %")

#     score_TPRFPR = ((abs(before_info_a[0]-before_info_b[0])-abs(after_info_a[0]-after_info_b[0]))/abs(before_info_a[0]-before_info_b[0]) + 
#                     (abs(before_info_a[1]-before_info_b[1])-abs(after_info_a[1]-after_info_b[1]))/abs(before_info_a[1]-before_info_b[1]))/2
#     # print("Equalized odds : " + str(score_TPRFPR*100) + " %")

#     score_ED = (abs(before_info_a[3]-before_info_b[3])-abs(after_info_a[3]-after_info_b[3]))/abs(before_info_a[3]-before_info_b[3])
#     # print("Demographic Parity : " + str(score_DP*100) + " %")

#     score_TE = (abs(before_info_a[4]-before_info_b[4])-abs(after_info_a[4]-after_info_b[4]))/abs(before_info_a[4]-before_info_b[4])
#     # print("Treatment Equal : " + str(score_TR*100) + " %")

# #     avg_score = (score_TPR+score_TPRFPR+score_ED+score_TE)/4
# # #     print(
# # #           " Avg : "+str(avg_score*100)[:5] + " %" 
# # #         + " Equal Opportunity : "+str(score_TPR*100)[:5] + " %" 
# # #         + " Equalized Odd : "+str(score_TPRFPR*100)[:5] + " %" 
# # #         + " Equalizing Disincentives : "+str(score_ED*100)[:5] + " %" 
# # #         + " Treatment Equal : "+str(score_TE*100)[:5] + " %"
# # #          )
# # #     print("--------------------------------------------------------")
    
#     return score_TPR, score_TPRFPR, score_ED, score_TE



########## 정적보상 ##########

def get_avg( arg, ratio ) : 
    arg_sum = 0
    arg_len = len(arg)
    avg_ratio=[]
    avg_fair=[]
    
    for i in range(arg_len) : 
        arg_sum+=(len(arg[i])*ratio)
    arg_avg = arg_sum/arg_len
        
    for i in range(arg_len) : 
        if len(arg[i])>arg_avg : 
            avg_fair.append(arg_avg)
        else : avg_fair.append(len(arg[i]))
            
    return arg_avg, avg_fair

def get_first_data( df, cur_col, tpra, tprb , fpra, fprb ) : 
    uniq = df[cur_col].unique()
    return_li = []
    for i in uniq : 
        return_li.append(df[df[cur_col]==i])

    arg_len = len(return_li)
    avg_ratio=abs(abs(tpra-tprb)+abs(fpra-fprb))/2
    avg_fair=[]
    arg_sum = 0
    
    for i in range(arg_len) : 
        arg_sum+=len(return_li[i])
    arg_avg = arg_sum/arg_len
    
    for i in range(arg_len) : 
        if len(return_li[i])>arg_avg : 
            avg_fair.append(len(return_li[i])-(len(return_li[i])-arg_avg)*avg_ratio)
        else : avg_fair.append(len(return_li[i]))
            
    return return_li, avg_fair

def get_next_data( df, cur_col, ratio ) : 
    uniq = df[cur_col].unique()
    return_li = []
    for i in uniq : 
        return_li.append(df[df[cur_col]==i])
    arg_avg, avg_fair= get_avg(return_li, ratio)
    return return_li, avg_fair



def static_fair(avg_fair, fair_data, col):

    tmp_fair_data=[]
    tmp_avg_li=[]

    for i,j in zip(fair_data,avg_fair) : 
        if j < len(i) : 
            ratio=1-((len(i)-j)/len(i))
        else : ratio=1
        #print(ratio)        
        return_li,avg = get_next_data(i, col, ratio)

        tmp_avg_li.append(avg)
        tmp_fair_data.append(return_li)

    tmp_fair_data=list(itertools.chain(*tmp_fair_data))
    #print(len(b))

    avg_fair=list(itertools.chain(*tmp_avg_li))
    
    return avg_fair, tmp_fair_data




########## 동적보상 ##########

def combine_next_col_uniq(data, arg, col) :
    uniq_list=[]
    for i in arg : 
        for j in data[col].unique() : 
            tmp=i.copy()
            tmp.append(j)
            uniq_list.append(tmp)

    return uniq_list

def get_batch_avg(result_list) : 
    sum_0=0
    non_0=0
    for i in result_list[0] : 
        sum_0+=len(result_list[0][i])
        if len(result_list[0][i]) !=0:
            non_0+=1
    avg_0 = int(sum_0/non_0) 

    sum_1=0
    non_1=0
    for i in result_list[1] : 
        sum_1+=len(result_list[1][i])
        if len(result_list[1][i]) !=0:
            non_1+=1
    avg_1 = int(sum_1/non_1)
    return avg_0, avg_1


def make_subset(data, target, uniq_list, group_list) : 
    result_list=[]
    for tg in data[target].unique() :
        df_list=dict()
        for tr in uniq_list :
            tmp = data[data[target]==tg].copy()
            for col, uniq in zip(group_list, tr)  : 
                tmp=tmp[tmp[col]==uniq].copy()
            df_list[tr]=tmp
        result_list.append(df_list)
    return result_list


def batch_append(batch, result_list, uniq_list, avg_0, avg_1) : 
    
    batch_list=[]
    remain_result_list=[]
    
    df_list=dict()
    re_list=dict()
    for uniq in uniq_list : 
        tmp = result_list[0][uniq]
        
        if len(tmp) >= avg_0 :
            avg_sample=tmp.sample(avg_0)
        else : 
            avg_sample=tmp.copy()
        
        df_list[uniq]=avg_sample
        re_list[uniq]=pd.DataFrame()
        result_list[0][uniq].drop(avg_sample.index, inplace=True)
    
    batch_list.append(df_list)
    remain_result_list.append(re_list)
    
    df_list=dict()
    re_list=dict()
    for uniq in uniq_list : 
        tmp = result_list[1][uniq]
        
        if len(tmp) >= avg_1 :
            avg_sample=tmp.sample(avg_1)
        else : 
            avg_sample=tmp.copy()
            
        #avg_sample=tmp.copy()
        
        df_list[uniq]=avg_sample
        re_list[uniq]=pd.DataFrame()
        result_list[1][uniq].drop(avg_sample.index, inplace=True)
    
    batch_list.append(df_list)
    remain_result_list.append(re_list)
    
    remain=pd.DataFrame()
    for uniq in uniq_list : 
        remain=pd.concat([remain,result_list[0][uniq]])
        remain=pd.concat([remain,result_list[1][uniq]])
        
    remain=pd.concat([remain, batch])

    return batch_list, remain, remain_result_list


def make_fair(data, target, except_target, batch) : 
    
    group_list = np.array(except_target)
    
    uniq_list=data[except_target].groupby(list(group_list)).count()
    uniq_list = dict(zip(uniq_list.index.values, uniq_list.values))

    result_list = make_subset(data, target, uniq_list, group_list)
    avg_0, avg_1 = get_batch_avg(result_list) 
        
    batch_list, remain, remain_result_list = batch_append(batch, result_list, uniq_list, avg_0, avg_1)
    
    remain_uniq_list=remain[except_target].groupby(list(group_list)).count()
    remain_uniq_list = dict(zip(remain_uniq_list.index.values, remain_uniq_list.values))
    
    for tg in remain[target].unique() :
        df_list=dict()
        for tr in remain_uniq_list :
            tmp = remain[remain[target]==tg].copy()
            for col, uniq in zip(group_list, tr)  : 
                tmp=tmp[tmp[col]==uniq].copy()
            df_list[tr]=tmp
            remain_result_list[tg][tr] = tmp
    
    test_list = remain_uniq_list
    for i in uniq_list : 
        test_list.pop(i, [])
        
    for i in test_list : 
        batch_list[0][i] = remain_result_list[0][i].copy()
        batch_list[1][i] = remain_result_list[1][i].copy()
        
        remain_result_list[0][i].drop(batch_list[0][i].index, inplace=True)
        remain_result_list[1][i].drop(batch_list[1][i].index, inplace=True)
    
    uniq_list.update(remain_uniq_list)
    
    for i in uniq_list : 
        if len(batch_list[0][i]) > len(batch_list[1][i]) : 
            dif = len(batch_list[0][i]) - len(batch_list[1][i])
            if len(remain_result_list[1][i]) > dif : 
                tmp = remain_result_list[1][i].sample(dif)
                batch_list[1][i] = pd.concat([batch_list[1][i],tmp])
                remain_result_list[1][i].drop(tmp.index, inplace=True)
            else :
                batch_list[1][i] = pd.concat([batch_list[1][i],remain_result_list[1][i]])
                remain_result_list[1][i].drop(remain_result_list[1][i].index, inplace=True)
            
        if len(batch_list[1][i]) > len(batch_list[0][i]) : 
            dif = len(batch_list[1][i]) - len(batch_list[0][i])
            if len(remain_result_list[0][i]) > dif : 
                tmp = remain_result_list[0][i].sample(dif)
                batch_list[0][i] = pd.concat([batch_list[0][i],tmp])
                remain_result_list[0][i].drop(tmp.index, inplace=True)
            else :
                batch_list[0][i] = pd.concat([batch_list[0][i],remain_result_list[0][i]])
                remain_result_list[0][i].drop(remain_result_list[0][i].index, inplace=True)

    min_size=len(data)
    
    for i in result_list :
        for j in i :
            if len(i[j]) > 0 :
                if len(i[j]) < min_size : 
                    min_size=len(i[j])
    
    min_list = []
    for i in result_list :
        for j in i :
            min_tmp = []
            if len(i[j]) > min_size :
                min_tmp.append(j)
        min_tmp = set(min_tmp)
        min_list.append(min_tmp)
                            
    if min_size > 0 :            
        for i in range(len(remain_result_list)) :
            for j in min_list[i] : 
                tmp = remain_result_list[i][j].sample(min_size)
                remain_result_list[i][j].drop(tmp.index, inplace=True)
                batch_list[i][j] = pd.concat([batch_list[i][j], tmp])
                

    train=pd.DataFrame()
    for i in uniq_list : 
        train=pd.concat([train,batch_list[0][i], batch_list[1][i]])

            
    remain=pd.DataFrame()
    for i in uniq_list : 
        remain=pd.concat([remain,remain_result_list[0][i], remain_result_list[1][i]])     
            
    return train, remain

def dynamic_fair(train, batch, num, target) : 
        
    cols = list(train.columns)
    
    except_target = cols.copy()
    except_target.remove(target)

    train, remain = make_fair(train, target, except_target, batch)

    num+=1
    
    return train, remain, num